﻿using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Stahovac
{    class Program
    {
        static void Main(string[] args)
        {
            string puvodniOdkaz = "https://www.rocketjumpninja.com/";
            
            List<string> Odkazy= new List<string>();
            Odkazy.Add(puvodniOdkaz);

            HtmlWeb web = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc = web.Load(puvodniOdkaz);

            // extracting all links
            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
            {
                HtmlAttribute att = link.Attributes["href"];

                if (att.Value.StartsWith("https") && !Odkazy.Contains(att.Value))
                {
                    Odkazy.Add(att.Value.ToString());
                    Console.WriteLine(att.Value);
                }
            }
            for (int i = 0; i < 2; i++)
            {
                foreach (var odkaz in Odkazy.ToList())
                {
                    HtmlWeb webFor = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument docFor = new HtmlAgilityPack.HtmlDocument();
                    docFor = web.Load(odkaz);

                    // extracting all links
                    try
                    {
                        foreach (HtmlNode link in docFor.DocumentNode.SelectNodes("//a[@href]"))
                        {
                            HtmlAttribute att = link.Attributes["href"];

                            if (att.Value.StartsWith("https") && !Odkazy.Contains(att.Value))
                            {
                                Odkazy.Add(att.Value);
                                Console.WriteLine(att.Value.ToString());
                                Downloader down = new Downloader();
                                _ = down.WithUrl(att.Value.ToString()).WithStorage("D:\\programko\\").Run();
                                Thread.Sleep(3);

                            }
                        }
                    }
                    catch { }

                }
                Console.WriteLine("done");


            }
            Console.ReadKey();
        }
    }
    class Downloader
    {
        ImmutableList<string> Kolekce = ImmutableList.Create<string>();
        public string cesta;
        public Downloader()
        {

        }

        public Downloader WithUrl(string url)
        {
            var klon = new Downloader(this);
            klon.Kolekce = klon.Kolekce.Add(url);
            return klon;
        }
        public Downloader WithStorage(string storage)
        {
            var klon = new Downloader(this);
            klon.cesta = storage;
            return klon;
        }
        private Downloader(Downloader previous)
        {
            this.cesta = previous.cesta;
            this.Kolekce = previous.Kolekce;
        }
        public async Task Run()
        {
            using (var wc = new WebClient())
            {
                List<Task> tasks = new List<Task>();
                foreach (var item in Kolekce)
                {

                    var fileName = item.Replace('\\', '-').Replace('/', '-').Replace(':', '_');
                    var filePath = Path.Combine(cesta, fileName);
                    Task task = wc.DownloadFileTaskAsync(new Uri(item), filePath);
                    tasks.Add(task);

                }
                await Task.WhenAll(tasks);
            }

        }
         public async Task<string> Stranka()
         {
             HttpClient hc = new HttpClient();
             HttpResponseMessage result = await hc.GetAsync($"https://www.delta-skola.cz/");

             Stream stream = await result.Content.ReadAsStreamAsync();

             HtmlDocument doc = new HtmlDocument();

             doc.Load(stream);

             HtmlNodeCollection links = doc.DocumentNode.SelectNodes("//a[@href]");//the parameter is use xpath see: https://www.w3schools.com/xml/xml_xpath.asp 
             Console.WriteLine(links.ToString());
             return links.ToString();
        }
    }
}